// ==UserScript==
// @name         AutoTrimps-Zek (thronified)
// @version      1.1-Zek-throne
// @namespace    https://gl.git.rest/throne3d/autotrimps/raw/gh-pages/
// @updateURL    https://gl.git.rest/throne3d/autotrimps/raw/gh-pages/.user.js
// @description  Automate all the trimps!
// @author       zininzinin, spindrjr, Ishkaru, genBTC, Zeker0, throne3d
// @include      *trimps.github.io*
// @include      *kongregate.com/games/GreenSatellite/trimps
// @connect      *Zorn192.github.io/AutoTrimps*
// @connect      *trimps.github.io*
// @connect      self
// @grant        none
// ==/UserScript==

var script = document.createElement('script');
script.id = 'AutoTrimps-Zek';
//This can be edited to point to your own Github Repository URL.
script.src = 'https://gl.git.rest/throne3d/autotrimps/raw/gh-pages/AutoTrimps2.js'; //https://Zorn192.github.io/AutoTrimps/AutoTrimps2.js
//script.setAttribute('crossorigin',"use-credentials");
script.setAttribute('crossorigin', "anonymous");
document.head.appendChild(script);
